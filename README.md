[![Codacy Badge](https://app.codacy.com/project/badge/Grade/09bf7869f295446aab81db12115d304e)](https://www.codacy.com/gl/GNR092/steamcmdssh-alpine/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=GNR092/steamcmdssh-alpine&amp;utm_campaign=Badge_Grade)
[![Docker Pulls](https://img.shields.io/docker/pulls/gnr092/steamcmdssh.svg)](https://hub.docker.com/r/gnr092/steamcmdssh)
[![Image Size](https://img.shields.io/docker/image-size/gnr092/steamcmdssh/latest.svg)](https://hub.docker.com/r/gnr092/steamcmdssh)
[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

# steamcmdssh alpine v3.14

steamcmd and  ssh server

## Usage

### Pull latest image

```shell
docker pull gnr092/steamcmdssh:latest
```

### docker-compose

1. Copy docker-compose.yml.example to docker-compose.yml and update as needed. See example below:
[Docker-compose](https://docs.docker.com/compose/install/) example:

```yaml
version: '3'
services:
    steamcmdssh:
        container_name: steamcmdssh
        image: gnr092/steamcmdssh:latest
        environment: 
            PASS: 'S3CR3T0' #Password user root ssh
        volumes: 
            - './games:/home/steamcmd/games'
        ports: 
            - "2022:2022/tcp"
```

2. Run `docker-compose up -d` to build and start

## Connect ssh

1.-Open a terminal or ssh manager and enter the local ip or the remote ip is on a server

```shell
ssh root@entertheiphere -p 2022
```

connect with the password that I enter in the docker-compose 

2.-Once inside, type 

```shell
steamcmd 
```

to update the client and enjoy !!! 

3.-You can use the force_install_dir /home/steamcmd/games to install the games. 

## License

[MIT license](LICENSE)
