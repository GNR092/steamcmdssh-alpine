FROM alpine:3.14 AS base
LABEL mantainer 'GNR092 <genercan092@gmail.com>'
#ssh password
ENV PASS ''
# Set environment variables
ENV USER root
ENV HOME /home/steamcmd
ENV DEPS bash openrc rsync openssh
# Set working directory
WORKDIR $HOME

# Install prerequisites
RUN apk update \
    && apk add --no-cache ${DEPS} \
    && mkdir -p /run/openrc \
    && touch /run/openrc/softlevel

VOLUME [ "/home/steamcmd/games" ]

#Steamcmd lib and bins
RUN mkdir -p ${HOME} \
    && wget -qO- https://github.com/GNR092/g092/raw/master/lib.tar.gz | tar xvzf - \
    && mkdir -p /usr/linux32 \
    && cp i386-linux-gnu/* /lib/ \
    && cp linux32/* /usr/linux32/ \
    && cp steamcmd /usr/bin/steamcmd \
    && rm -rf i386-linux-gnu \
    && rm -rf linux32 \
    && rm steamcmd \
    && sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config \
    && sed -ie 's/#Port 22/Port 2022/g' /etc/ssh/sshd_config \
    && sed -ie 's/#PrintMotd yes/PrintMotd no/g' /etc/ssh/sshd_config \
    && sed -ie 's/X11Forwarding no/X11Forwarding yes/g' /etc/ssh/sshd_config \
    && sed -ie 's/GatewayPorts no/#GatewayPorts no/g' /etc/ssh/sshd_config \
    && /usr/bin/ssh-keygen -A \
    && ssh-keygen -t rsa -b 4096 -f  /etc/ssh/ssh_host_key \
    && echo "export VISIBLE=now" >> /etc/profile \
    && mkdir -p ${HOME}/games \
    && rm -rf /var/cache/apk/*

COPY ./entry.sh ${HOME}/entry.sh
VOLUME [ "${HOME}" ]
VOLUME [ "/sys/fs/cgroup" ]
ENV NOTVISIBLE "in users profile"
EXPOSE 2022/tcp

ENTRYPOINT [ "/bin/bash","entry.sh" ]